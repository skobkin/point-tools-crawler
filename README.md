# README #

### What is Point Tools Crawler for? ###

Point Tools Crawler was written for dumping Point.im posts to Point Tools service.
 
### How to build? ###

You need Go 1.6+ installed to build this project.

```bash
go get bitbucket.org/skobkin/point-tools-crawler
cd $GOPATH/src/bitbucket.org/skobkin/point-tools-crawler
go build
```

### How to use? ###

Just run point-tools-crawler binary with its CLI parameters:

```bash
./point-tools-crawler -l your_point_login -p your_point_password -t your_point_tools_crawler_api_token
```